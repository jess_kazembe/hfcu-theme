<?php get_header(); ?>

<?php //get_template_part( 'front-page-slider', get_post_format() ); ?>

<div class="content">
	<?php get_template_part('content', get_post_format() ); ?>
</div>

<?php get_footer(); ?>