<?php get_header(); ?>

<div class="content">
	<?php get_template_part( 'content', get_post_format() ); ?>
</div>
<?php get_footer(); ?>
		