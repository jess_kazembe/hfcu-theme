            <footer>
    			<div class="links">
    				<div class="column">
    					<h4>Become a Member</h4>
    					<?php wp_nav_menu( array( 
							'theme_location' => 'member-menu', 
							'menu_class' => 'menu' 
						) ); ?>
    				</div>
    				<div class="column">
    					<h4>Proposed HFCU</h4>
    					<?php wp_nav_menu( array( 
							'theme_location' => 'footer-menu', 
							'menu_class' => 'menu' 
						) ); ?>
    				</div>
    			</div>
    
    			<div class="legal">
    				&copy; 2019 Proposed Hebrews Federal Credit Union – A Naheb Project
    			</div>
            </footer>
        </div>
        <?php wp_footer();?>
	</body>
</html>